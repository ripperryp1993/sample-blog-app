from django.db import models

# Create your models here.


class Post(models.Model):  # One
    title = models.CharField(max_length=100)
    created = models.DateTimeField()
    content = models.TextField()


class Comment(models.Model):  # Many. Need to add foreign key
    author = models.CharField(max_length=100)
    created = models.DateTimeField()
    content = models.TextField(max_length=100)
    post = models.ForeignKey(
        "Post", related_name="comments", on_delete=models.CASCADE
    )


class Keyword(models.Model):
    word = models.CharField(max_length=20)
    posts = models.ManyToManyField('Post', related_name='keywords')